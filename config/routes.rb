Rails.application.routes.draw do
  apipie
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :identities, path: '', controllers: { sessions: 'identities/sessions' }

  resources :character, only: [] do
    resources :traits, only: [:create, :destroy], controller: 'character_traits'
    resources :archetypes, only: [:new, :create, :destroy], controller: 'character_archetypes'
    resources :items, except: [:index, :show], controller: 'character_items'
    resources :spells, controller: 'character_spells'
    resources :descriptions, except: [:index, :show, :destroy], controller: 'character_descriptions'
  end

  namespace :admins do
    resource :dashboards, only: :show
    resources :users do
      resource :user_block, only: [:create, :destroy], as: 'block'
    end
    resources :release_notes, except: [:show]
    resources :encounters
    resources :parties do
      resource :rest, only: :update, controller: 'rest'
      resource :party_experience, only: [:edit, :update], as: 'experience'
    end

    scope module: :characters do
      resources :archetypes
      resources :adventurers
      # resources :monsters
    end

    scope module: :handbooks do
      resources :rules, except: :show
      resources :traits, except: :show
      resources :items, controller: 'base_items'
      resources :spells
      resources :races
    end

    scope module: :masteries do
      resources :abilities, except: :show
      resources :disciplines, except: :show
      resources :supports, except: :show
    end
  end

  scope module: :members do
    resource :dashboards, only: :show
    resources :adventurers
  end

  scope module: :handbooks do
    resources :rules, only: [:index, :new, :create]
    resources :traits, only: [:index, :new, :create]
    resources :spells, only: [:index, :new, :create, :show]
    resources :races, only: :index
    resources :items, only: [:index, :show], controller: 'base_items'
    resources :monsters, only: [:index, :show]
  end

  scope module: :characters do
    resources :archetypes, only: [:index, :show]
    resources :races, only: [:index, :show]
    resources :damages, only: :update
    resources :heals, only: :update
  end

  namespace :char_points, as: :char_point do
    resources :base, only: [:edit, :update], as: ''
    resources :increment, only: :update
    resources :decrement, only: :update
    resources :experiences, only: :update
  end

  namespace :api do
    devise_for :identities, path: '', only: [:sessions], controllers: { sessions: 'api/auth/sessions' }
    resources :adventurers, only: [:index, :show]
  end

  get 'news', to: 'news_feed#show', as: 'news'
  get 'search',  to: 'search#show', as: :search
  get 'autocomplete', to: 'search#autocomplete', as: :autocomplete

  authenticated :identity do
    root to: 'members/dashboards#show'
  end

  devise_scope :identity do
    root to: 'news_feed#show', as: :unsigned_root
  end
end
