require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

class DreamLogFormatter < Logger::Formatter
  def call(severity, time, progname, msg)
    "[%s(%d)%5s] %s\n" % [time.to_s(:short), $$, severity, msg2str(msg)]
  end
end

module Krpg
  class Application < Rails::Application
    config.autoload_paths << File.join(config.root, 'lib')
    config.autoload_paths += Dir[Rails.root.join('app', 'models', '{**}')]
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    config.assets.paths << Rails.root.join('vendor', 'assets', 'images')

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :en
    # config.i18n.available_locales = [:en, :ru]
    I18n.config.enforce_available_locales = true

    config.logger = ActiveSupport::Logger.new(Rails.root.join('log', "#{Rails.env}.log"), 10, 30*1024*1024)
    config.logger.level = ActiveSupport::Logger::ERROR
    config.logger.formatter = DreamLogFormatter.new

    config.time_zone = 'UTC'

    config.generators do |g|
      g.template_engine :haml
      g.test_framework :rspec
    end
  end
end
