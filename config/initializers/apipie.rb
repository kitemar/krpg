Apipie.configure do |config|
  config.app_name                = "KRPG"
  config.copyright               = "&copy; #{Time.now.year} Aleksey Lavrukhin"
  config.api_base_url            = "/api"
  config.doc_base_url            = "/apidoc"
  # config.app_info["1.0"]         = "This is where you can inform user about your application and API in general."
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
end
