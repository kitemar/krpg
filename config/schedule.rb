set :output, { error: 'cron.error.log', standard: 'cron.log' }
env :MAILTO, 'team@kitemar.com'

job_type :runner,
         "cd :path && RAILS_ENV=:environment $HOME/.rbenv/bin/rbenv exec bundle exec rake :task :output"

every 1.hours do
  runner 'adventurers:cleaner'
end
