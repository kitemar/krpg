module Admins
  module Users
    class IndexPage < ::GeneralPage
      set_url '/admins/users'

      class UserRow < ::SitePrism::Section
        element :name, 'td:nth-child(1)'
        element :email, 'td:nth-child(2)'
        element :admin, 'td:nth-child(3)'
        element :active, 'td:nth-child(4)'
        element :destroy_btn, 'td:nth-child(5) a'
      end

      sections :users_rows, UserRow, 'table tbody tr'
    end
  end
end
