module Admins
  module Users
    class ShowPage < ::GeneralPage
      set_url '/admins/users/{user_id}'

      element :name_field, '#user_name'
      element :email_field, 'user_email'
      element :admin_checkbox, '#user_admin'
      element :block_btn, '.block-btn'
      element :unblock_btn, '.unblock-btn'
    end
  end
end
