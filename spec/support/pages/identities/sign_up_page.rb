module Identities
  class SignUpPage < GeneralPage
    set_url '/sign_up'

    class SignUpForm < GeneralForm
      element :name,      '#identity_name'
      element :email,     '#identity_email'
      element :password,  '#identity_password'
      element :confirm,   '#identity_password_confirmation'

      def sign_up_with attrs
        name.set      attrs[:name]
        email.set     attrs[:email]
        password.set  attrs[:password]
        confirm.set   attrs[:confirm]
        submit.click
      end
    end

    section :form, SignUpForm, '#new_identity'
    delegate :sign_up_with, :name, :email, :password, :confirm, to: :form
  end
end
