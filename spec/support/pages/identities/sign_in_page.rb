module Identities
  class SignInPage < GeneralPage
    set_url '/sign_in'

    element :email, '#identity_email'
    element :password, '#identity_password'
    element :forgot_password, '//a[contains(@text, "Forgot your password?")]'

    def login_with(email, password)
      self.email.set(email)
      self.password.set(password)
      submit.click
    end
  end
end
