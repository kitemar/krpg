class GeneralForm < SitePrism::Section
  element :submit, 'input[name="commit"]'

  def set_with(params, except: [])
    params.except(*except).each do |element, value|
      send(element).set value
    end
  end
end
