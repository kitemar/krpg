FactoryGirl.define do
  factory :user, class: Identity do
    sequence(:email) { |n| "user_#{n}@example.com" }
    password 'password'

    after(:build) do |identity|
      identity.build_user email: identity.email,
                          name: Faker::Name.name,
                          admin: false
    end
  end

  factory :admin, parent: :user do
    after(:create) { |identity| identity.user.update_attributes(admin: true) }
  end
end
