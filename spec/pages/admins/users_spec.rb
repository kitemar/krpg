feature 'Users Page', js: true, type: :feature do
  let!(:admin) { create :admin }
  let!(:user) { create :user }
  let(:index_page) { Admins::Users::IndexPage.new }
  let(:user_page) { Admins::Users::ShowPage.new }

  before { login_as admin }

  describe '#index page' do
    before { index_page.load }

    it { expect(index_page).to be_displayed }
    it { expect(index_page.users_rows.count).to eq(2) }
    it { expect(index_page.users_rows.first.admin.text).to eq('true') }

    it 'destroy active user' do
      index_page.users_rows.last.destroy_btn.click
      expect(index_page.alert_warning.text).to eq('× User was successfully locked')
      expect(index_page.users_rows.last.active.text).to eq('false')
      expect(index_page.users_rows.count).to eq(2)
    end

    it 'destroy inactive user' do
      user.lock_access!
      index_page.users_rows.last.destroy_btn.click
      expect(index_page.alert_warning.text).to eq('× User was successfully removed')
      expect(index_page.users_rows.count).to eq(1)
    end
  end

  describe '#show page' do
    let(:locked_user) { create :user, locked_at: Time.now }
    before { user_page.load user_id: user.id }

    it { expect(user_page).to be_displayed }

    it 'block user' do
      user_page.block_btn.click
      wait_for_ajax
      expect(user_page.has_unblock_btn?).to be_truthy
    end

    it 'unblock user' do
      user_page.load user_id: locked_user.id
      user_page.unblock_btn.click
      wait_for_ajax
      expect(user_page.has_block_btn?).to be_truthy
    end

    it 'update user name' do
      user_page.name_field.set 'New name'
      user_page.submit.click
      expect(user_page.name_field.value).to eq('New name')
    end
  end
end
