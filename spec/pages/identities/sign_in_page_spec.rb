feature 'Sign in page', js: true, type: :feature do
  let(:sign_in_page) { Identities::SignInPage.new }
  let(:page) { GeneralPage.new }
  let(:user) { create :user }
  let(:admin) { create :admin }

  before do
    sign_in_page.load
  end

  it { expect(sign_in_page).to be_displayed }

  it '#allow to sign in' do
    sign_in_page.login_with(user.email, user.password)
    expect(page.alert_info.text).to eq('× Signed in successfully.')
    expect(page.has_admin?).to be_falsey
  end

  it 'login as admin' do
    sign_in_page.login_with(admin.email, admin.password)
    expect(page.alert_info.text).to eq('× Signed in successfully.')
    expect(page.has_admin?).to be_truthy
  end

  it '#login with incorrect data' do
    sign_in_page.login_with('fail@example.com', 'password')
    expect(sign_in_page.alert_error.text).to eq('× Invalid Email or password.')
    expect(sign_in_page.has_sign_out_link?).to be_falsey
  end

  it 'login as locked user' do
    user.lock_access!
    sign_in_page.login_with(user.email, user.password)
    expect(page.alert_error.text).to eq('× Your account is locked.')
    expect(page.has_sign_out_link?).to be_falsey
  end

  it '#login and logout' do
    sign_in_page.login_with(user.email, user.password)
    sign_in_page.sign_out_link.click
    expect(page.alert_info.text).to eq('× Signed out successfully.')
  end
end
