feature 'Sign Up Page', js: true, type: :feature do
  let(:sign_up_page) { Identities::SignUpPage.new }
  let(:page) { GeneralPage.new }

  let(:params) {{ name: 'User',
                  email: 'user@mailinator.com',
                  password: 'password',
                  confirm: 'password' }}

  before do
    sign_up_page.load
  end

  it { expect(sign_up_page).to have_submit }
  it { expect(sign_up_page.current_url).to include '/sign_up' }
  it { expect(sign_up_page.has_sign_out_link?).to be_falsey }

  it 'allow signup with correct data' do
    sign_up_page.sign_up_with params
    expect(page.alert_info.text).to eq('× Welcome! You have signed up successfully.')
    expect(page.has_sign_out_link?).to be_truthy
  end

  it 'with empty data' do
    sign_up_page.submit.click
    expect(sign_up_page).to be_displayed
  end
end
