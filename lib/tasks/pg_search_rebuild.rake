require 'active_record'

namespace :db do
  desc 'pg_search rebuild'
  task pg_search_rebuild: [:environment] do
    [Rule, Trait, Spell, Monster, Race, BaseItem].each do |model|
      PgSearch::Multisearch.rebuild(model)
    end
    p 'rebuilding finished'
  end
end
