module Auth::TokenDoc
  def self.included(base)
    base.instance_eval do
      header 'X-Identity-Token', 'User token'
      header 'X-Identity-Email', 'User email'
      error code: 401, desc: 'Token and Email is invalid'
    end
  end
end
