module Auth::PasswordDoc
  def self.included(base)
    base.instance_eval do
      param :identity, Hash, required: true do
        param :email, String, required: true
        param :password, String, required: true
      end
      error code: 401, desc: 'Invalid Email or password.'
    end
  end
end
