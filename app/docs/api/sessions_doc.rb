module Api
  module SessionsDoc
    extend BaseDoc

    namespace 'api/auth/'
    resource :sessions

    resource_description do
      short 'Users session'
      formats ['json']
      error code: 406, desc: 'Unknown Format'
    end

    doc_for :create do
      auth_with :password
      api :POST, '/sign_in', 'Create new user session'
      param :token, String
      example <<-EOS
        {
          "auth_token": "FWPNhxTMyEH_d-CAm_as",
          "user": {
            "email": "user2@example.com",
            "name": "Test user"
          }
        }
      EOS
    end

    doc_for :destroy do
      auth_with :token
      api :DELETE, '/sign_out', 'Destroy user session'
      param :token, String
      example <<-EOS
        {
          "success": "Signed out"
        }
      EOS
    end
  end
end
