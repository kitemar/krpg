module Api
  module AdventurersDoc
    extend BaseDoc

    namespace '/api'
    resource :adventurers

    resource_description do
      short 'Adventurers'
    end

    doc_for :index do
      api :GET, '/adventurers', 'Index all playable adventurers for current user (all playable for admin)'
      example <<-EOS
      SUCCESS:
      {
        "adventurers": [
          {
            "id": 11,
            "name": "Test adventurer",
          },
          {
            "id": 14,
            "name": "Something",
          }
        ]
      }
      EOS
    end

    doc_for :show do
      api :GET, '/adventurers/:id', 'Show specific adventurer'
      example <<-EOS
      SUCCESS:
      {
        "info": {
        "id": 14,
        "name": "Something",
        "speed": 30,
        "initiative": 4
        }
      }
      EOS
    end
  end
end
