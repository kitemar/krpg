class CharacterItemsController < Members::BaseController
  def new; end

  def edit; end

  def create
    if new_item.update_attributes(item_params)
      flash[:success] = 'Item was successfully created!'
      redirect_to current_user.admin? ? polymorphic_path([:admins, character]) : adventurer_path(character)
    else
      render :new
    end
  end

  def update
    if item.update_attributes(item_params)
      respond_to :js
    else
      render :edit
    end
  end

  def destroy
    item.destroy
    flash[:success] = 'Item was successfully removed!'
    respond_to { |format| format.js { render inline: "location.reload();" } }
  end

  private

  helper_method :new_item
  def new_item
    @new_tem ||= CharacterItem.new(character: character, active: true)
  end

  helper_method :item
  def item
    @item ||= CharacterItem.find(params[:id])
  end

  helper_method :base_items
  def base_items
    @base_items ||= BaseItem.ordered.active.where(default: false)
  end

  helper_method :character
  def character
    @character ||= Character.find(params[:character_id])
  end

  def item_params
    params.require(:character_item).permit(:title, :effect, :properties, :kind, :weight, :price, :quantity)
  end
end
