class SearchController < ApplicationController
  include ApplicationHelper

  def show
    if params[:query].present?
      results = PgSearch.multisearch(params[:query]).sort_by{|x| ["Rule", "Trait", "Spell", "Character", "Race", "Item"].index x.searchable_type}
      @search_entities = results.map{|entity| entity.searchable_type.constantize.find(entity.searchable_id)}
    end
  end

  def autocomplete
    results = PgSearch.multisearch(params[:term]).sort_by{|x| ["Rule", "Trait", "Spell", "Character", "Race", "Item"].index x.searchable_type}.take(10)
    render json: results.map { |s| { name: content(s),
                                    type: s.searchable_type,
                                    anchor_link: anchor_link(content(s)),
                                    anchor_resourсe: anchor_resourсe(s.searchable_type),
                                    anchor_resourсe_id: s.searchable_id }}
  end

  private

  def content(term)
    if term.searchable_type == 'Rule'
      term.searchable_type.classify.safe_constantize.find(term.searchable_id).title
    else
      term.content
    end
  end
end
