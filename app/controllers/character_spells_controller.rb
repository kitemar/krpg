class CharacterSpellsController < Members::BaseController
  def new; end

  def create
    if new_character_spell.update_attributes(spell_params)
      flash[:success] = 'Spell was successfully added!'
      redirect_to current_user.admin? ? polymorphic_path([:admins, character]) : adventurer_path(character)
    else
      render :new
    end
  end

  def destroy
    character_spell.destroy
    flash[:warning] = 'Spell was successfully removed.'
    respond_to { |format| format.js { render inline: "location.reload();" } }
  end

  private

  helper_method :character_spell
  def character_spell
    @character_spell ||= CharacterSpell.find_by!(character: character, spell: spell)
  end

  helper_method :character
  def character
    @character ||= Character.find(params[:character_id])
  end

  helper_method :spells
  def spells
    @spells ||= Spell.accepted.where.not(id: character.spell_ids)
  end

  helper_method :spell
  def spell
    @spell ||= Spell.find(params[:id])
  end

  helper_method :new_character_spell
  def new_character_spell
    @new_character_spell ||= CharacterSpell.new(character: character)
  end

  def spell_params
    params.require(:character_spell).permit(:spell_id)
  end
end
