module Handbooks
  class SpellsController < BaseController
    def index
      respond_to do |format|
        format.html
        format.js { @spells ||= Spell.accepted.where(group: params[:group]) }
      end
    end

    def show
      respond_to :js
      @spell ||= Spell.find(params[:id])
    end

    def new; end

    def create
      if new_spell.update_attributes(spell_params)
        flash[:success] = 'Spell was successfully proposed!'
        redirect_to spells_url
      else
        render :new
      end
    end

    private

    helper_method :spells
    def spells
      @spells ||= Spell.accepted.order(:group).order(:title).group_by(&:group)
    end

    helper_method :proposed_spells
    def proposed_spells
      @proposed_spells ||= Spell.proposed.proposed_by(current_user).order(:group).order(:title)
    end

    helper_method :new_spell
    def new_spell
      @new_spell ||= Spell.new(accepted: false, user: current_user)
    end

    def spell_params
      params.require(:spell).permit(:title, :description, :group)
    end
  end
end
