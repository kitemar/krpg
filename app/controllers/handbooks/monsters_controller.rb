module Handbooks
  class MonstersController < BaseController
    include Calculable

    def index; end

    def show; end

    private

    helper_method :monsters
    def monsters
      @monsters ||= $monsters.sort_by { |m| m[:info][:name] }
    end

    helper_method :monster
    def monster
      @monster ||=  $monsters.find { |x| x[:info][:name] == params[:id].downcase }
    end

    helper_method :max_hit_points
    def max_hit_points(monster)
      calc_hp(monster[:abilities][:con].to_i,
              monster[:info][:rank].to_i,
              monster[:hit_points][:bonus].to_i)
    end
  end
end
