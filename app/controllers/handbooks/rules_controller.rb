module Handbooks
  class RulesController < BaseController
    def index; end
    def new; end

    def create
      if new_rule.update_attributes(rule_params)
        flash[:success] = 'Rule was successfully proposed!'
        redirect_to rules_url
      else
        render :new
      end
    end

    private

    helper_method :rules
    def rules
      @rules ||= Rule.accepted.order(:group).order(:title).group_by(&:group)
    end

    helper_method :proposed_rules
    def proposed_rules
      @proposed_rules ||= Rule.proposed.proposed_by(current_user).order(:group).order(:title)
    end

    helper_method :new_rule
    def new_rule
      @new_rule ||= Rule.new(group: 'Базовые правила', accepted: false, user: current_user)
    end

    def rule_params
      params.require(:rule).permit(:title, :description, :group)
    end
  end
end
