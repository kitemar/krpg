module Handbooks
  class BaseItemsController < BaseController
    def index
      respond_to do |format|
        format.html
        format.js { @items ||= BaseItem.ordered.active.where(kind: params[:kind]) }
      end
    end

    def show
      # respond_to :js
      # @item ||= items.find(params[:id])

      respond_to do |format|
        format.html
        format.js { @item ||= base_items.find(params[:id]) }
      end
    end

    private

    helper_method :base_items
    def base_items
      @base_items ||= BaseItem.ordered.active.order(:id)
    end

    helper_method :items
    def items
      @items ||= Item.ordered.active.order(:id)
    end

    helper_method :item
    def item
      @item ||= items.find(params[:id])
    end
  end
end
