module Handbooks
  class BaseController < ::ApplicationController
    before_action :authenticate_identity!, only: [:new, :create]
  end
end
