module Handbooks
  class TraitsController < BaseController
    def index; end
    def new; end

    def create
      if new_trait.update_attributes(trait_params)
        flash[:success] = 'Trait was successfully proposed!'
        redirect_to traits_url
      else
        render :new
      end
    end

    private

    helper_method :new_trait
    def new_trait
      @new_trait ||= Trait.new(playable: false, accepted: false, user: current_user)
    end

    helper_method :traits
    def traits
      @traits ||= Trait.accepted.where(playable: true).order(:points).order(:title).joins(:mastery).group_by(&:mastery)
    end

    helper_method :proposed_traits
    def proposed_traits
      @proposed_traits ||= Trait.proposed.proposed_by(current_user).order(:points).order(:title).joins(:mastery)
    end

    helper_method :masteries
    def masteries
      @masteries ||= Mastery.active.where(type: %w(Ability Discipline))
    end

    def trait_params
      params.require(:trait).permit(:title, :description, :condition, :points, :mastery_id)
    end
  end
end
