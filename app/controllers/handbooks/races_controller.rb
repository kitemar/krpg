module Handbooks
  class RacesController < BaseController
    def index; end

    helper_method :races
    def races
      @races ||= Race.playable
    end
  end
end
