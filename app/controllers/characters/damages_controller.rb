module Characters
  class DamagesController < Admins::BaseController
    respond_to :js

    def update
      character.damage!(params[:damage][:value].to_i)
    end

    private

    helper_method :character
    def character
      @character ||= Character.find(params[:id])
    end
  end
end
