module Characters
  class HealsController < Admins::BaseController
    respond_to :js

    def update
      if params[:use_hd].present?
        character.use_hit_dice!
      else
        character.heal!(params[:heal][:value].to_i)
      end
    end

    private

    helper_method :character
    def character
      @character ||= Character.find(params[:id])
    end
  end
end
