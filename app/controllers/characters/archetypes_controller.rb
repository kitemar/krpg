module Characters
  class ArchetypesController < Members::BaseController
    respond_to :js

    def index; end
    def show; end

    private

    helper_method :archetypes
    def archetypes
      @archetypes ||= Archetype.all
    end

    helper_method :archetype
    def archetype
      @archetype ||= Archetype.find(params[:id])
    end
  end
end
