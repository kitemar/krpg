module Characters
  class RacesController < Members::BaseController
    respond_to :js

    def index; end
    def show; end

    private

    helper_method :races
    def races
      @races ||= Race.all
    end

    helper_method :race
    def race
      @race ||= Race.find(params[:id])
    end
  end
end
