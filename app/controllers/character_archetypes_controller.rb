class CharacterArchetypesController < Members::BaseController

  def new; end

  def create
    if character.add_archetype(new_archetype)
      new_character_archetype.update_attributes(character_archetype_params)
      flash[:success] = 'Archetype was successfully added!'
      redirect_to current_user.admin? ? polymorphic_path([:admins, character]) : adventurer_path(character)
    else
      render :new
    end
  end

  def destroy
    character.remove_archetype(archetype)
    character_archetype.destroy
    flash[:success] = 'Archetype was successfully removed!'
    respond_to { |format| format.js { render inline: "location.reload();" } }
  end

  private

  helper_method :new_character_archetype
  def new_character_archetype
    @new_character_archetype ||= CharacterArchetype.new(character: character)
  end

  helper_method :character_archetype
  def character_archetype
    @character_archetype ||= CharacterArchetype.find_by!(character: character, archetype: archetype)
  end

  helper_method :new_archetype
  def new_archetype
    @new_archetype ||= Archetype.find(character_archetype_params['archetype_id'])
  end

  helper_method :character
  def character
    @character ||= Character.find(params[:character_id])
  end

  helper_method :archetype
  def archetype
    @archetype ||= Archetype.find(params[:id])
  end

  helper_method :archetypes
  def archetypes
    @archetypes ||= (current_user.admin ? Archetype.all : Archetype.playable).where.not(id: character.archetype_ids)
  end

  def character_archetype_params
    params.require(:character_archetype).permit(:archetype_id)
  end
end
