module Admins
  class UserBlocksController < BaseController
    respond_to :js

    def create
      user.identity.lock_access!
    end

    def destroy
      user.identity.unlock_access!
    end

    private

    helper_method :user
    def user
      @user ||= User.find(params[:user_id])
    end
  end
end
