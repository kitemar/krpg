module Admins
  class BaseController < Members::BaseController
    before_action :require_admin!

    private

    def require_admin!
      return redirect_to root_path, flash: { error: 'Access denied!' } unless current_user.admin?
    end
  end
end
