module Admins
  class DashboardsController < BaseController
    def show; end

    private

    helper_method :release_notes
    def release_notes
      @release_notes ||= ReleaseNote.all.order(created_at: :desc)
    end

    helper_method :new_release_note
    def new_release_note
      @new_release_note ||= ReleaseNote.new
    end

    helper_method :proposed_rules
    def proposed_rules
      @proposed_rules ||= Rule.proposed.count
    end

    helper_method :proposed_traits
    def proposed_traits
      @proposed_traits ||= Trait.proposed.count
    end

    helper_method :proposed_spells
    def proposed_spells
      @proposed_spells ||= Spell.proposed.count
    end

    helper_method :adventurers
    def adventurers
      @adventurers ||= Adventurer.where(playable: true)
                                 .joins(:user)
                                 .where(users: { admin: false })
                                 .order(created_at: :desc)
                                 .limit(3)
    end

    helper_method :parties
    def parties
      @parties ||= Party.all.order(updated_at: :desc)
    end

    helper_method :encounters
    def encounters
      @encounters ||= Encounter.all.order(updated_at: :desc)
    end
  end
end
