module Admins
  module Handbooks
    class RacesController < BaseController
      def index; end
      def new; end
      def edit; end
      def show; end

      def create
        if new_race.update_attributes(race_params)
          flash[:success] = 'Race was successfully created!'
          redirect_to admins_races_path
        else
          render :new
        end
      end

      def update
        if race.update_attributes(race_params)
          flash[:success] = 'Race was successfully updated!'
          redirect_to admins_races_path
        else
          render :edit
        end
      end

      def destroy
        if race.archetype.nil? && race.adventurers.blank?
          race.destroy
          flash[:success] = 'Race was successfully removed'
          redirect_to admins_races_path
        else
          flash[:warning] = 'Race cannot be removed'
          redirect_to admins_races_path
        end
      end

      private

      helper_method :new_race
      def new_race
        @new_race ||= Race.new
      end

      helper_method :race
      def race
        @race ||= Race.find(params[:id])
      end

      helper_method :races
      def races
        @races ||= Race.all.order(:id)
      end

      def race_params
        params.require(:race).permit(:code, :playable)
      end
    end
  end
end
