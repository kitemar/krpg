module Admins
  module Handbooks
    class RulesController < BaseController
      def index; end
      def new; end
      def edit; end

      def create
        if new_rule.update_attributes(rule_params)
          flash[:success] = 'Rule was successfully created!'
          redirect_to admins_rules_url
        else
          render :new
        end
      end

      def update
        if rule.update_attributes(rule_params)
          flash[:success] = 'Rule was successfully changed!'
          redirect_to admins_rules_url
        else
          render :edit
        end
      end

      def destroy
        rule.destroy
        flash[:warning] = 'Rule was successfully removed!'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :new_rule
      def new_rule
        @new_rule ||= Rule.new(group: 'Базовые правила', accepted: true)
      end

      helper_method :rule
      def rule
        @rule ||= Rule.find(params[:id])
      end

      helper_method :proposed_rules
      def proposed_rules
        @proposed_rules ||= Rule.proposed.order(:group).order(:title)
      end

      helper_method :rules
      def rules
        @rules ||= Rule.accepted.order(:group).order(:title).group_by(&:group)
      end

      helper_method :users
      def users
        @users ||= User.all
      end

      def rule_params
        params.require(:rule).permit(:title, :description, :group, :proposer_id, :accepted)
      end
  end
  end
end
