module Admins
  module Handbooks
    class BaseItemsController < BaseController
      def index; end
      def new; end
      def edit; end

      def create
        if new_item.update_attributes(item_params)
          flash[:success] = 'Item was successfully created!'
          redirect_to admins_items_path
        else
          render :new
        end
      end

      def update
        if item.update_attributes(item_params)
          flash[:success] = 'Item was successfully updated!'
          redirect_to admins_items_path
        else
          render :edit
        end
      end

      private

      helper_method :new_item
      def new_item
        @new_item ||= BaseItem.new
      end

      helper_method :item
      def item
        @item ||= BaseItem.find(params[:id])
      end

      helper_method :items
      def items
        @items ||= BaseItem.ordered
      end

      def item_params
        params.require(:base_item).permit(:title, :effect, :kind, :properties, :weight, :price, :active, :default)
      end
  end
  end
end
