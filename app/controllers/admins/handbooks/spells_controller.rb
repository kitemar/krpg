module Admins
  module Handbooks
    class SpellsController < BaseController
      def index; end
      def new; end
      def edit; end

      def create
        if new_spell.update_attributes(spell_params)
          flash[:success] = 'Spell was successfully created!'
          redirect_to admins_spells_path
        else
          render :new
        end
      end

      def update
        if spell.update_attributes(spell_params)
          flash[:success] = 'Spell was successfully updated!'
          redirect_to admins_spells_path
        else
          render :edit
        end
      end

      def destroy
        spell.destroy
        flash[:success] = 'Spell was successfully removed!'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :new_spell
      def new_spell
        @new_spell ||= Spell.new
      end

      helper_method :spell
      def spell
        @spell ||= Spell.find(params[:id])
      end

      helper_method :proposed_spells
      def proposed_spells
        @proposed_spells ||= Spell.proposed.order(:group).order(:title)
      end

      helper_method :users
      def users
        @users ||= User.all
      end

      helper_method :spells
      def spells
        @spells ||= Spell.accepted.order(:group).order(:title).group_by(&:group)
      end

      def spell_params
        params.require(:spell).permit(:title, :group, :description, :proposer_id, :accepted)
      end
    end
  end
end
