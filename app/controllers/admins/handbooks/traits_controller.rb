module Admins
  module Handbooks
    class TraitsController < BaseController
      def index; end
      def new; end
      def edit; end

      def create
        if new_trait.update_attributes(trait_params)
          flash[:success] = 'Trait was successfully created!'
          redirect_to admins_traits_url
        else
          render :new
        end
      end

      def update
        if trait.update_attributes(trait_params)
          flash[:success] = 'Trait was successfully changed!'
          redirect_to admins_traits_url
        else
          render :edit
        end
      end

      def destroy
        trait.destroy
        flash[:warning] = 'Trait was successfully removed!'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :new_trait
      def new_trait
        @new_trait ||= Trait.new(accepted: true)
      end

      helper_method :trait
      def trait
        @trait ||= Trait.find(params[:id])
      end

      helper_method :proposed_traits
      def proposed_traits
        @proposed_traits ||= Trait.proposed.order(:points).order(:title).joins(:mastery)
      end

      helper_method :traits
      def traits
        @traits ||= Trait.accepted.order(:points).order(:title).joins(:mastery).group_by(&:mastery)
      end

      helper_method :masteries
      def masteries
        @masteries ||= Mastery.all
      end

      helper_method :users
      def users
        @users ||= User.all
      end

      def trait_params
        params.require(:trait).permit(:title, :description, :condition, :points, :mastery_id, :proposer_id, :playable, :accepted)
      end
    end
  end
end
