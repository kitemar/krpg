module Admins
  class RestController < BaseController
    def update
      adventurers.each(&:rest!)
      respond_to { |format| format.js { render inline: "location.reload();" } }
    end

    private

    helper_method :party
    def party
      @party ||= Party.find(params[:party_id])
    end

    helper_method :adventurers
    def adventurers
      @adventurers ||= party.characters
    end
  end
end
