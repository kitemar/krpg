module Admins
  class UsersController < BaseController
    def index; end
    def show; end

    def update
      user.update_attributes(user_params)
    end

    def destroy
      if user.active?
        user.identity.lock_access!
        flash[:warning] = 'User was successfully locked'
      else
        user.destroy
        flash[:warning] = 'User was successfully removed'
      end
      respond_to { |format| format.js { render inline: "location.reload();" } }
    end

    private

    helper_method :users
    def users
      @users ||= User.all.order(admin: :desc)
    end

    helper_method :user
    def user
      @user ||= User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :admin, identity_attributes: [:chat_id])
    end
  end
end
