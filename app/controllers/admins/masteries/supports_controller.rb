module Admins
  module Masteries
    class SupportsController < Masteries::BaseController
      def index
      end

      def new
      end

      def create
        if new_support.update_attributes(supports_params)
          flash[:success] = 'Support was successfully created!'
          redirect_to admins_supports_path
        else
          render :new
        end
      end

      def edit
      end

      def update
        if support.update_attributes(supports_params)
          flash[:success] = 'Support was successfully changed!'
          redirect_to admins_supports_path
        else
          render :edit
        end
      end

      def destroy
        support.destroy
        flash[:warning] = 'Support was successfully removed.'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :new_support
      def new_support
        @new_support ||= Support.new
      end

      helper_method :support
      def support
        @support ||= Support.find(params[:id])
      end

      helper_method :supports
      def supports
        @supports ||= Support.all
      end

      def supports_params
        params.require(:support).permit(:code,:active)
      end
    end
  end
end
