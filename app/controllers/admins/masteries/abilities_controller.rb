module Admins
  module Masteries
    class AbilitiesController < Masteries::BaseController
      def index
      end

      def new
      end

      def create
        if new_ability.update_attributes(abilities_params)
          flash[:success] = 'Ability was successfully created!'
          redirect_to admins_abilities_path
        else
          render :new
        end
      end

      def edit
      end

      def update
        if ability.update_attributes(abilities_params)
          flash[:success] = 'Ability was successfully changed!'
          redirect_to admins_abilities_path
        else
          render :edit
        end
      end

      def destroy
        ability.destroy
        flash[:warning] = 'Ability was successfully removed.'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :new_ability
      def new_ability
        @new_ability ||= Ability.new
      end

      helper_method :ability
      def ability
        @ability ||= Ability.find(params[:id])
      end

      helper_method :abilities
      def abilities
        @abilities ||= Ability.all
      end

      def abilities_params
        params.require(:ability).permit(:code,:active)
      end
    end
  end
end
