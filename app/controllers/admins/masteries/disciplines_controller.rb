module Admins
  module Masteries
    class DisciplinesController < Masteries::BaseController
      def index; end
      def new; end
      def edit; end

      def create
        if new_discipline.update_attributes(discipline_params)
          flash[:success] = 'Discipline was successfully created!'
          redirect_to admins_disciplines_path
        else
          render :new
        end
      end

      def update
        if discipline.update_attributes(discipline_params)
          flash[:notice] = 'Discipline was successfully changed!'
          redirect_to admins_disciplines_path
        else
          render :edit
        end
      end

      def destroy
        discipline.destroy
        flash[:warning] = 'Ability was successfully removed.'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :new_discipline
      def new_discipline
        @new_discipline ||= Discipline.new
      end

      helper_method :discipline
      def discipline
        @discipline ||= Discipline.find(params[:id])
      end

      helper_method :disciplines
      def disciplines
        @disciplines ||= Discipline.all
      end

      def discipline_params
        params.require(:discipline).permit(:code,:active)
      end
    end
  end
end
