module Admins
  class ReleaseNotesController < BaseController
    def index; end
    def new; end
    def edit; end

    def create
      if new_release_note.update_attributes(release_note_params)
        flash[:success] = 'Release note was successfully published!'
        redirect_to dashboards_path
      else
        render :new
      end
    end

    def update
      if release_note.update_attributes(release_note_params)
        flash[:success] = 'Release note was successfully updated!'
        redirect_to dashboards_path
      else
        render :edit
      end
    end

    def destroy
      release_note.destroy
      flash[:success] = 'Release note was successfully removed.'
      respond_to { |format| format.js { render inline: "location.reload();" } }
    end

    private

    helper_method :release_notes
    def release_notes
      @release_notes ||= ReleaseNote.all.order(created_at: :desc)
    end

    helper_method :new_release_note
    def new_release_note
      @new_release_note ||= ReleaseNote.new(user: current_user)
    end

    helper_method :release_note
    def release_note
      @release_note ||= ReleaseNote.find(params[:id])
    end

    def release_note_params
      params.require(:release_note).permit(:title, :content, :owner_id)
    end
  end
end
