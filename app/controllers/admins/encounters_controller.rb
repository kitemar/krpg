module Admins
  class EncountersController < BaseController
    def index; end
    def new; end
    def edit; end
    def show; end

    def create
      if new_encounter.update_attributes(encounter_params)
        flash[:success] = 'Encounter was successfully created!'
        redirect_to admins_encounter_path(new_encounter)
      else
        render :new
      end
    end

    def update
      if encounter.update_attributes(encounter_params)
        flash[:success] = 'Encounter was successfully updated!'
        redirect_to admins_encounter_path(encounter)
      else
        render :edit
      end
    end

    def destroy
      encounter.destroy
      respond_to { |format| format.js { render inline: "location.reload();" } }
    end

    private

    helper_method :encounters
    def encounters
      @encounters ||= Encounter.all
    end

    helper_method :new_encounter
    def new_encounter
      @new_encounter ||= Encounter.new
    end

    helper_method :monsters
    def monsters
      @monsters ||= $monsters.sort_by { |m| m[:info][:name] }
    end

    helper_method :encounter
    def encounter
      @encounter ||= Encounter.find(params[:id])
    end

    def encounter_params
      params.require(:encounter).permit(:title, monsters: [:name, :count])
    end
  end
end
