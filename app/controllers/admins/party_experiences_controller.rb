module Admins
  class PartyExperiencesController < BaseController
    include Calculable
    def edit; end

    def update
      party.characters.each { |adventurer| update_character(adventurer) }
      redirect_to admins_party_path(party)
    end

    private

    helper_method :party
    def party
      @party ||= Party.find(params[:party_id])
    end

    def update_character(adventurer)
      adventurer.mastery('experience').add_points!(experience(adventurer)) if adventurer.active_in_party?
      adventurer.mastery('downtime').add_points!(downtime)
      logger.warn "(game info) Character '#{adventurer.name}' gains #{experience(adventurer)} experiences and #{downtime.to_i} downtime."
    end

    def experience(character)
      experience = (params[:party][:experience].to_f / party.active_characters.count).round(3)
      calc_exp(experience, party.rank, character.rank)
    end

    def downtime
      params[:party][:downtime].to_i + params[:party][:experience].to_i / 10
    end

    def party_params
      params.require(:party).permit(:experience, :downtime)
    end
  end
end
