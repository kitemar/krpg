module Admins
  module Characters
    class AdventurersController < BaseController
      def index; end
      def new; end

      def show
        render 'members/adventurers/charsheet' if params[:print]
        redirect_to edit_admins_adventurer_url(adventurer) if adventurer.editable
      end

      def edit
        redirect_to admins_adventurer_url(adventurer) if adventurer.playable
      end

      def create
        if new_adventurer.update_attributes(adventurer_params)
          redirect_to edit_admins_adventurer_url(new_adventurer)
        else
          render :new
        end
      end

      def update
        if params['adventurer'].present?
          adventurer.update_attributes(adventurer_params)
        else
          adventurer.update_attributes(playable: (adventurer.playable ? false : true))
        end
        redirect_to admins_adventurer_url(adventurer)
      end

      def destroy
        adventurer.destroy
        flash[:danger] = 'Adventurer removed.'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :npcs
      def npcs
        @npcs ||= Adventurer.joins(:user).where(users: { admin: true })
      end

      helper_method :adventurers
      def adventurers
        @adventurers ||= Adventurer.joins(:user).where(users: { admin: false })
      end

      helper_method :new_adventurer
      def new_adventurer
        @new_adventurer ||= Adventurer.new(playable: false)
      end

      helper_method :adventurer
      def adventurer
        @adventurer ||= Adventurer.find(params[:id])
      end

      helper_method :races
      def races
        @races ||= Race.all
      end

      helper_method :users
      def users
        @users ||= User.all
      end

      def adventurer_params
        params.require(:adventurer).permit(:name, :race_id, :user_id, :playable, :description)
      end
    end
  end
end
