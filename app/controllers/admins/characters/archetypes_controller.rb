module Admins
  module Characters
    class ArchetypesController < BaseController
      def index; end
      def show; end
      def new; end
      def edit; end

      def create
        if new_archetype.update_attributes(archetype_params)
          flash[:success] = 'Archetype was successfully created!'
          redirect_to admins_archetype_url(new_archetype)
        else
          render :new
        end
      end

      def update
        if archetype.update_attributes(archetype_params)
          flash[:success] = 'Archetype was successfully changed!'
          redirect_to admins_archetype_url(archetype)
        else
          render :edit
        end
      end

      def destroy
        archetype.destroy
        flash[:warning] = 'Archetype was successfully removed.'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :archetypes
      def archetypes
        @archetypes ||= Archetype.all.order(:id)
      end

      helper_method :new_archetype
      def new_archetype
        @new_archetype ||= Archetype.new(playable: false)
      end

      helper_method :archetype
      def archetype
        @archetype ||= Archetype.find(params[:id])
      end

      helper_method :races
      def races
        @races ||= Race.all
      end

      def archetype_params
        params.require(:archetype).permit(:name, :race_id, :playable, :description)
      end
    end
  end
end
