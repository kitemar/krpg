module Admins
  module Characters
    class MonstersController < BaseController
      def index; end
      def new; end

      def show
        redirect_to edit_admins_monster_url(monster) if monster.editable
      end

      def edit
        redirect_to admins_monster_url(monster) if monster.playable
      end

      def create
        if new_monster.update_attributes(monster_params)
          redirect_to edit_admins_monster_url(new_monster)
        else
          render :new
        end
      end

      def update
        if monster.update_attributes(monster_params)
          redirect_to admins_monster_url(monster)
        else
          render :edit
        end
      end

      def destroy
        monster.destroy
        flash[:warning] = 'Monster removed.'
        respond_to { |format| format.js { render inline: "location.reload();" } }
      end

      private

      helper_method :monsters
      def monsters
        @monsters ||= Monster.all.order(:name)
      end

      helper_method :new_monster
      def new_monster
        @new_monster ||= Monster.new(playable: false)
      end

      helper_method :monster
      def monster
        @monster ||= Monster.find(params[:id])
      end

      def monster_params
        params.require(:monster).permit(:name, :description, :playable)
      end
    end
  end
end
