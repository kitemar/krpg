module Admins
  class PartiesController < BaseController
    def index; end
    def show; end
    def new; end
    def edit; end

    def create
      if new_party.update_attributes(party_params)
        flash[:success] = 'Party was successfully created!'
        redirect_to admins_party_path(new_party)
      else
        render :new
      end
    end

    def update
      if party.update_attributes(party_params)
        flash[:success] = 'Party was successfully updated!'
        redirect_to admins_party_path(party)
      else
        render :edit
      end
    end

    def destroy
      party.destroy
      respond_to { |format| format.js { render inline: "location.reload();" } }
    end

    private

    helper_method :parties
    def parties
      @parties ||= Party.all
    end

    helper_method :new_party
    def new_party
      @new_party ||= Party.new
    end

    helper_method :party
    def party
      @party ||= Party.find(params[:id])
    end

    helper_method :adventurers_list
    def adventurers_list
      @adventurers_list ||= Adventurer.playable.without_party
    end

    helper_method :party_adventurers
    def party_adventurers
      @party_adventurers ||= party.characters.order(:id)
    end

    helper_method :abilities
    def abilities
      @abilities ||= CharPoint.abilities.order(:mastery_id).includes(:mastery).order(:character_id).where(character_id: party_adventurers.ids).group_by(&:mastery)
    end

    helper_method :disciplines
    def disciplines
      @disciplines ||= CharPoint.disciplines.order(:mastery_id).includes(:mastery).order(:character_id).where(character_id: party_adventurers.ids).group_by(&:mastery)
    end

    def party_params
      params.require(:party).permit(:title, affiliations_attributes: [:id, :character_id, :active, :_destroy])
    end
  end
end
