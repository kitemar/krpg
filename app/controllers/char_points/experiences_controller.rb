module CharPoints
  class ExperiencesController < CharPoints::BaseController
    def update
      character_experience.dec!
      char_point.add_exp!
      render 'char_points/update'
    end

    private

    helper_method :character_experience
    def character_experience
      @character_experience ||= char_point.character.mastery('experience')
    end
  end
end
