module CharPoints
  class BaseController < ApplicationController
    respond_to :js

    def edit; end

    def update
      char_point.update_points_to(params[:char_point][:points])
      render 'char_points/update'
    end

    private

    helper_method :char_point
    def char_point
      @char_point ||= CharPoint.find(params[:id])
    end
  end
end
