module CharPoints
  class DecrementController < BaseController
    def update
      char_point.dec!
      render 'char_points/update'
    end
  end
end
