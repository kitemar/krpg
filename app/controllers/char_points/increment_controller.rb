module CharPoints
  class IncrementController < BaseController
    def update
      char_point.inc!
      render 'char_points/update'
    end
  end
end
