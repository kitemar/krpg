class Api::Auth::SessionsController < Devise::SessionsController
  include SessionsDoc
  acts_as_token_authentication_handler_for Identity, only: [:destroy], fallback: :exception

  skip_before_action :verify_authenticity_token, if: -> { request.format.json? }
  skip_before_action :verify_signed_out_user, if: -> { request.format.json? }

  respond_to :json

  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message!(:notice, :signed_in)
    sign_in(resource_name, resource)
    resource.user.tokens.find_or_create_by value: params[:token] if params[:token]
    yield resource if block_given?
  end

  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    if signed_out
      set_flash_message! :notice, :signed_out
    end
    yield if block_given?
  end

  private

  def handle_unverified_request
    raise ActionController::UnknownFormat
  end

  def resource_name
    :identity
  end
end
