module Api
  class BaseController < ::ApplicationController
    acts_as_token_authentication_handler_for Identity, fallback: :exception
    skip_before_action :verify_authenticity_token, if: -> { request.format.json? }

    respond_to :json
  end
end
