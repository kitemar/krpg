module Api
  class AdventurersController < BaseController
    include AdventurersDoc

    def index; end
    def show; end

    private

    helper_method :adventurers
    def adventurers
      @adventurers ||= current_user.admin ? Adventurer.playable : current_user.adventurers.playable
    end

    helper_method :adventurer
    def adventurer
      @adventurer ||= adventurers.find params[:id]
    end

    helper_method :mastery
    def mastery(id)
      @mastery ||= Mastery.find(id)
    end
  end
end
