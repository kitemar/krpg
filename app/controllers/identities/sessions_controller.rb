class Identities::SessionsController < Devise::SessionsController
  respond_to :html, :js

  def create
    params[:identity].merge!(remember_me: 1)
    super
  end
end
