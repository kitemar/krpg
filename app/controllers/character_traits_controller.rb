class CharacterTraitsController < Members::BaseController
  respond_to :js

  def create
    character.character_traits.create!(trait_id: params[:trait])
    render inline: "location.reload();"
  end

  def destroy
    character_trait.destroy
    flash[:warning] = 'Trait was successfully removed.'
    render inline: "location.reload();"
  end

  private

  helper_method :character_trait
  def character_trait
    @character_trait ||= CharacterTrait.find_by!(character: character, trait: trait)
  end

  helper_method :character
  def character
    @character ||= Character.find(params[:character_id])
  end

  helper_method :trait
  def trait
    @trait ||= Trait.find(params[:id])
  end
end
