module Members
  class DashboardsController < BaseController
    def show
      redirect_to admins_dashboards_url if current_user.try(:admin)
    end

    private

    helper_method :adventurers
    def adventurers
      @adventurers ||= current_user.adventurers.order(updated_at: :desc)
    end
  end
end
