module Members
  class AdventurersController < BaseController
    def index
      redirect_to admins_adventurers_path if current_user.admin
    end

    def new; end

    def show
      render 'charsheet' if params[:print]
      redirect_to edit_adventurer_url(adventurer) if adventurer.editable
    end

    def edit
      redirect_to adventurer_url(adventurer) if adventurer.playable
    end

    def create
      if new_adventurer.update_attributes(adventurer_params)
        redirect_to edit_adventurer_url(new_adventurer)
      else
        render :new
      end
    end

    def update
      if params['adventurer'].present?
        adventurer.update_attributes(adventurer_params)
      else
        adventurer.update_attributes(playable: (adventurer.playable ? false : true))
      end
      redirect_to adventurer_url(adventurer)
    end

    def destroy
      adventurer.destroy
      flash[:danger] = 'Adventurer removed.'
      respond_to { |format| format.js { render inline: "location.reload();" } }
    end

    private

    helper_method :adventurers
    def adventurers
      @adventurers ||= current_user.adventurers
    end

    helper_method :new_adventurer
    def new_adventurer
      @new_adventurer ||= Adventurer.new(user: current_user, playable: false)
    end

    helper_method :adventurer
    def adventurer
      @adventurer ||= adventurers.find(params[:id])
    end

    helper_method :races
    def races
      @races ||= Race.playable
    end

    def adventurer_params
      params.require(:adventurer).permit(:name, :race_id, :description)
    end
  end
end
