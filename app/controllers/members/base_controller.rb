module Members
  class BaseController < ::ApplicationController
    before_action :authenticate_identity!
  end
end
