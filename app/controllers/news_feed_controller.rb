class NewsFeedController < ApplicationController
  def show; end

  private

  helper_method :release_notes
  def release_notes
    @release_notes ||= ReleaseNote.all.order(created_at: :desc)
  end
end
