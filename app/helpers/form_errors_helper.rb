module FormErrorsHelper
  include ActionView::Helpers::FormOptionsHelper
  ActionView::Helpers::FormBuilder.send :include, FormErrorsHelper

  def error_span(attribute, options = {})
    options[:span_class] ||= 'form-control-feedback'
    options[:error_class] ||= 'has-danger'

    if errors_on?(attribute)
      @template.content_tag( :div, class: options[:error_class] )  do
        content_tag( :div, errors_for(attribute), class: options[:span_class] )
      end
    end
  end

  def errors_on?(attribute)
    object.errors[attribute].present? if object.respond_to?(:errors)
  end

  def errors_for(attribute)
    object.errors[attribute].try(:join, ', ') || object.errors[attribute].try(:to_s)
  end
end
