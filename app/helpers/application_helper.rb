module ApplicationHelper
  def anchor_link anchor
    anchor.mb_chars.downcase.to_s.gsub(/\,|\:|\s\(.*$/,'').gsub(/\s/, '_')
  end

  def anchor_resourсe anchor
    anchor.downcase.pluralize
  end

  def link_to_remove_fields(name, f, function)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "#{function}(this); return false;", class: 'btn btn-secondary')
  end

  def link_to_add_fields(name, f, association, function)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    content_tag :a, name, href: '#', onclick: "#{function}(this, '#{association}', '#{escape_javascript(fields)}'); return false;"
  end

  def link_to_add_monsters(name, f, association, function)
    fields = f.fields_for(association) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    content_tag :a, name,
                href: '',
                class: 'btn btn-sm btn-success',
                onclick: "#{function}(this, '#{association}', '#{escape_javascript(fields)}'); return false;"
  end

  def link_to_remove_monsters(name, function)
    link_to(name, '',class: 'fa fa-times fa--danger fa-2x', onclick: "#{function}(this); return false;")
  end
end
