class CharacterItem < Item
  validates :quantity, numericality: { greater_than_or_equal_to: 0}
  belongs_to :character, touch: true

  def weight_summary
    weight * quantity
  end
end
