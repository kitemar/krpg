class User < ApplicationRecord
  validates :name, presence: true
  validates :email, uniqueness: true, presence: true

  after_save :update_identity

  has_one :identity, dependent: :destroy
  has_many :adventurers, dependent: :destroy
  has_many :rules, foreign_key: :proposer_id, dependent: :nullify
  has_many :traits, foreign_key: :proposer_id, dependent: :nullify
  has_many :spells, foreign_key: :proposer_id, dependent: :nullify
  delegate :locked_at, to: :identity, prefix: false

  accepts_nested_attributes_for :identity, update_only: true
  delegate :chat_id, to: :identity, prefix: false

  def active?
    locked_at.nil?
  end

  private

  def update_identity
    identity.update_column(:email, email) if identity && identity.email != email
  end
end
