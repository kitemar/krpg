class CharacterTrait < ApplicationRecord
  belongs_to :character, touch: true
  belongs_to :trait
end
