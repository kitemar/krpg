class Encounter < ApplicationRecord
  include Calculable
  serialize :monsters, JSON

  validates :title, :monsters, presence: true
  before_save :filter_monsters

  def rank
    rank = 0
    monsters.each do |m|
      monster = monster(m['name'])
      rank += monster[:info][:rank].to_i * m['count'].to_i
    end
    rank
  end

  def monster(name)
    $monsters.find { |x| x[:info][:name] == name }
  end

  def max_hit_points(monster)
    calc_hp(monster(monster['name'])[:abilities][:con],
            monster(monster['name'])[:info][:rank].to_i,
            monster(monster['name'])[:hit_points][:bonus].to_i)
  end

  private

  def filter_monsters
    filter = []
    monsters.each do |m|
      if m['name'].present? && m['count'].to_i > 0
        m['name'].gsub!(/\s/, '_') || m['name']
        m['name'].downcase! || m['name']
        filter << m
      end
    end
    self.monsters = filter
  end
end
