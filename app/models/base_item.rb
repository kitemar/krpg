class BaseItem < Item
  include PgSearch
  multisearchable against: :title,
                  if: :active

  scope :default, -> { active.where(default: true) }

  def description
    "<p><b>Kind</b>: #{kind}<br />
    <b>Effect</b>: #{effect}<br />
    <b>Properties</b>: #{properties}<br />
    <b>Weight</b>: #{weight} kg<br />
    <b>Price</b>: #{price} gc</p>"
  end
end
