class CharPoint < ApplicationRecord
  belongs_to :character, touch: true
  belongs_to :mastery

  delegate :title, to: :mastery, prefix: false
  delegate :code, to: :mastery, prefix: false

  scope :abilities, -> { where(mastery: Ability.active) }
  scope :disciplines, -> { where(mastery: Discipline.active) }
  scope :supports, -> { where(mastery: Support.active) }

  before_save :experience_check

  def modifier
    mastery.type == 'Ability' ? points / 2 - 5 : points / 5
  end

  def add_exp!
    update_attributes(experience: experience + 1)
  end

  def add_points!(p)
    update_attributes(points: points + p)
  end

  def update_points_to(points)
    update_attributes(points: points.to_i)
  end

  def inc!
    update_attributes(points: points + 1)
  end

  def dec!
    update_attributes(points: points - 1)
  end

  private

  def experience_check
    return if experience < points + 1
    assign_attributes(points: points + 1, experience: 0)
  end
end
