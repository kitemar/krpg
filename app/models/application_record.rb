class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  private

  def strip
    attribute_names.each { |name| send("#{name}=", send(name).strip) if send(name).respond_to?(:strip) }
  end
end
