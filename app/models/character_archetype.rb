class CharacterArchetype < ApplicationRecord
  belongs_to :character, touch: true
  belongs_to :archetype
end
