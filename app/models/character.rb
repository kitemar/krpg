class Character < ApplicationRecord
  include Calculable

  before_validation :strip
  validates :name, presence: true

  has_many :char_points, dependent: :destroy
  has_many :masteries, through: :char_points
  has_many :ability_points, -> { CharPoint.abilities.order(:mastery_id) }, class_name: 'CharPoint'
  has_many :discipline_points, -> { CharPoint.disciplines.order(:mastery_id) }, class_name: 'CharPoint'
  has_many :support_points, -> { CharPoint.supports.order(:mastery_id) }, class_name: 'CharPoint'

  has_many :character_traits, dependent: :destroy
  has_many :traits, through: :character_traits
  has_many :character_archetypes, dependent: :destroy
  has_many :archetypes, through: :character_archetypes
  has_many :character_items, dependent: :destroy
  has_many :character_spells, dependent: :destroy
  has_many :spells, through: :character_spells

  has_one :affiliation, dependent: :destroy
  has_one :party, through: :affiliation

  accepts_nested_attributes_for :ability_points
  accepts_nested_attributes_for :discipline_points
  accepts_nested_attributes_for :support_points

  delegate :title, to: :race, prefix: true, allow_nil: true
  delegate :name, to: :user, prefix: true, allow_nil: true

  alias_attribute :title, :name

  scope :playable, -> { where(playable: true) }

  def add_archetype(archetype)
    points = archetype.char_points.map {|point| point.attributes.slice('points', 'mastery_id')}.group_by{|point| point['mastery_id'] }
    items = archetype.character_items.map {|item| item.attributes.except('id', 'character_id', 'created_at', 'updated_at')}
    char_points.each do |cp|
      cp.update_attributes(points: cp.points + points[cp.mastery.id][0]['points'])
    end
    character_items.create(items)
    update_attributes(description: description + archetype.description)
    archetype.traits.each { |trait| traits << trait unless traits.include?(trait) }
    archetype.spells.each { |spell| spells << spell unless spells.include?(spell) }
  end

  def remove_archetype(archetype)
    points = archetype.char_points.map { |item| item.attributes.slice('points', 'mastery_id')}.group_by{|item| item['mastery_id'] }
    char_points.each do |cp|
      cp.update_attributes(points: cp.points - points[cp.mastery.id][0]['points'])
    end
    archetype.traits.each { |trait| traits.delete(trait) }
    archetype.spells.each { |spell| spells.delete(spell) }
  end

  def info
    "#{name} - #{race_title} #{rank} rank's"
  end

  def editable
    !playable
  end

  def populate(archetype=nil)
    points = if archetype.present?
               archetype.char_points.map{|item| item.attributes.slice('points', 'mastery_id')}
             else
               Mastery.all.map {|item| { mastery_id: item.id, points: 0 }}
             end
    char_points.create!(points)
    if archetype.present?
      archetype.traits.pluck(:id).each{ |trait_id| self.character_traits.create!(trait_id: trait_id) }
      character_archetypes.create!(archetype: archetype)
      update_attributes(description: archetype.description)
    end
  end

  def rank(sum = 0)
    value = [sum]; discipline_points.each { |p| value << p.send(:modifier) }; value << value_for('rank', :points); value.sum
  end

  def playable_traits
    playable_traits = []
    Trait.where(playable: true).order(:title).each do |trait|
      if value_for(trait.code, :points) >= trait.points && !traits.include?(trait)
        playable_traits << trait
      end
    end
    playable_traits
  end

  def not_playable_traits
    not_playable_traits = []
    Trait.where(playable: false).order(:title).each do |trait|
      not_playable_traits << trait unless traits.include?(trait)
    end
    not_playable_traits
  end

  def mastery(code)
    CharPoint.find_by(character: self, mastery: Mastery.find_by(code: code))
  end

  def value_for(code, what) # points or modifier
    mastery = mastery(code)
    mastery.presence ? mastery.send(what) : 0
  end

  def value_summary(sum = 0, what) # points or modifier
    value = [sum]; discipline_points.each { |p| value << p.send(what) }; value.sum
  end

  def max_hit_points
    calc_hp(value_for('con', :points), rank, value_for('health_points', :points))
  end

  def max_soul_points # TODO: workaround
    magic ||= begin
      if value_for('arcane', :points) > value_for('divine', :points)
        value_for('arcane', :points)
      else
        value_for('divine', :points)
      end
    end
    (rank * 1.5).round + magic + value_for('soul_points', :points)
  end

  def initiative_bonus
    value_for('initiative', :points) + value_for('dex', :modifier)
  end

  def max_weight
    value_for('str', :points) * 7
  end
end
