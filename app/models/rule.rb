class Rule < ApplicationRecord
  include PgSearch
  multisearchable against: [:title, :description],
                  if: :accepted

  validates :title, :description, presence: true, uniqueness: true
  validates :group, presence: true

  belongs_to :user, foreign_key: :proposer_id
  delegate :name, to: :user, prefix: true, allow_nil: true
  scope :accepted, -> { where(accepted: true) }
  scope :proposed, -> { where(accepted: false) }
  scope :proposed_by, ->(user) { where(proposer_id: user.id) }
end
