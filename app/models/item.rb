class Item < ApplicationRecord
  validates :title, :kind, presence: true
  validates :weight, :price, numericality: { greater_than_or_equal_to: 0 }

  scope :ordered, -> { order(:kind).order(:title) }
  scope :active, -> { where(active: true) }
end
