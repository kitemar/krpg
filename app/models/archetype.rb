class Archetype < Character
  validates :name, presence: true, uniqueness: true
  belongs_to :user, required: false
  belongs_to :race, required: false
  after_create :populate
end
