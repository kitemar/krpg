class Party < ApplicationRecord
  has_many :affiliations, dependent: :destroy
  has_many :characters, through: :affiliations
  has_many :active_characters, -> { where(active: true) }, class_name: 'Affiliation'

  accepts_nested_attributes_for :affiliations, allow_destroy: true

  validates :title, presence: true

  def rank
    rank = 0.0
    characters.each do |adventurer|
      rank += adventurer.rank
    end
    (rank / characters.count).round(1)
  end

  def without_exp
    characters.all? { |adventurer| adventurer.value_for('experience', :points) == 0 }
  end
end
