class Mastery < ApplicationRecord
  validates :code, uniqueness: true, presence: true

  has_many :char_points, dependent: :destroy
  has_many :traits

  default_scope { order(:id) }
  scope :active, -> { where(active: true) }

  after_save :populate

  def title
    I18n.t("#{type}.#{code}", default: "#{code.titleize}")
  end

  def populate
    if active
      Character.all.each { |character| character.char_points.create!(mastery: self) unless character.char_points.find_by(mastery: self) }
    else
      CharPoint.where(mastery: self).destroy_all
    end
  end
end
