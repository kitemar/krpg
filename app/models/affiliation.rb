class Affiliation < ApplicationRecord
  belongs_to :party
  belongs_to :character

  scope :for_character, ->(character) { where(character_id: character.id) }
  scope :for_party, ->(party) { where(party_id: party.id) }

  validates :character, presence: true, uniqueness: true
end
