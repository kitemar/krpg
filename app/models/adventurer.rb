class Adventurer < Character
  belongs_to :race
  belongs_to :user

  scope :without_party, -> { where.not(id: Affiliation.select(:character_id)) }

  after_create :populate_from_race
  after_create :add_default_items

  def damage!(value)
    return if value < 1
    update_attributes(hit_points: hit_points - value)
  end

  def heal!(value)
    return if value < 1
    update_attributes(hit_points: hit_points + value > max_hit_points ? max_hit_points : hit_points + value)
  end

  def use_hit_dice!
    return if value_for('hit_dice', :points) < 1
    hit_dice = roll(1, hit_dice(value_for('con', :points))).sum
    heal!(hit_dice + value_for('con', :modifier))
    mastery('hit_dice').dec!
  end

  def rest!
    return if hit_points < 1
    half_hd = value_for('hit_dice', :points) + (rank / 2 < 1 ? 1 : rank / 2)
    update_attributes(hit_points:  max_hit_points)
    update_attributes(soul_points: max_soul_points)
    mastery('hit_dice').update_points_to(half_hd > rank ? rank : half_hd)
  end

  def dead
    hit_points <= (0 - max_hit_points)
  end

  def hit_dice_with_count
    "#{value_for('hit_dice', :points)}d#{hit_dice(value_for('con', :points))}"
  end

  def has_experience?
    value_for('experience', :points) > 0
  end

  def active_in_party?
    party_character.try(:active)
  end

  private

  def populate_from_race
    populate race.archetype
  end

  def add_default_items
    BaseItem.ordered.default.each do |item|
      character_items.create!(item.attributes.except('id', 'type').merge(active: true))
    end
  end
end
