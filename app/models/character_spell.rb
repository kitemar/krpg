class CharacterSpell < ApplicationRecord
  belongs_to :character, touch: true
  belongs_to :spell
end
