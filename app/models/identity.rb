class Identity < ApplicationRecord
  acts_as_token_authenticatable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :lockable

  attr_accessor :name

  belongs_to :user
  before_validation :create_user

  private

  def create_user
    user = User.create email: email, name: name
    self.user_id = user.id
  end
end
