module Calculable
  extend ActiveSupport::Concern

  def hit_dice(constitution)
    case constitution / 2 - 5
      when -5..-1 # =< 9
        4
      when 0..1 # 10..13
        6
      when 2 # 14..15
        8
      when 3 # 16..17
        10
      else
        12 # 18+
    end
  end

  def calc_hp(constitution, rank = 1, bonus = 0)
    constitution +
      (rank - 1) * (hit_dice(constitution) / 2 + 1) +
      (constitution / 2 - 5) * rank + bonus
  end

  def calc_exp(experience, party_rank, character_rank)
    difference = party_rank - character_rank
    percent = (difference.abs <= 5 ? difference * 20 : -100)
    exp_mod = experience.to_f / 100 * percent
    (experience + exp_mod).round
  end

  def roll(count, dice)
    result = []
    count.times do
      srand
      result << (1 + rand(dice))
    end
    logger.warn "(game info) rolled #{result.sum} (#{result.to_sentence})"
    result.sort!
  end
end
