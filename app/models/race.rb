class Race < ApplicationRecord
  include PgSearch
  multisearchable against: :title,
                  if: :playable

  validates :code, uniqueness: true, presence: true

  has_one :archetype
  delegate :name, to: :archetype, prefix: true, allow_nil: true
  delegate :spells, to: :archetype, allow_nil: true
  delegate :traits, to: :archetype, allow_nil: true
  delegate :character_items, to: :archetype, allow_nil: true
  delegate :ability_points, to: :archetype, allow_nil: true
  delegate :discipline_points, to: :archetype, allow_nil: true
  scope :playable, -> { where(playable: true) }

  def title
    I18n.t("race.#{code}", default: "#{code.titleize}")
  end

  def adventurers
    Adventurer.where(race_id: id)
  end
end
