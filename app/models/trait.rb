class Trait < ApplicationRecord
  include PgSearch
  multisearchable against: :title,
                  if: [:accepted, :playable]

  validates :title,
            :description,
            :points,
            presence: true
  belongs_to :mastery
  belongs_to :user, foreign_key: :proposer_id
  delegate :name, to: :user, prefix: true, allow_nil: true
  delegate :code, to: :mastery, prefix: false
  delegate :title, to: :mastery, prefix: true
  before_save :check_playable

  scope :accepted, -> { where(accepted: true) }
  scope :proposed, -> { where(accepted: false) }
  scope :proposed_by, ->(user) { where(proposer_id: user.id) }

  private

  def check_playable
    assign_attributes(playable: false) if mastery.type == 'Support'
  end
end
