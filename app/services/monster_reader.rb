class MonsterReader

  def initialize
    @path = Dir[Rails.root.join('app', 'supports', 'dictionaries', 'monsters', '**', '*.{rb,yml}').to_s]
    read
  end

  private

  def read
    $monsters = []
    @path.each do |file|
      $monsters << YAML.load_file(file).with_indifferent_access
    end
  end
end
