#= require ./index
App.Methods.Party = {}

App.Methods.Party.remove_fields = (link) ->
  $(link).prev("input[type=hidden]").val(1)
  $(link).parents(".input-group").first().hide()
  false
App.Methods.Party.add_fields = (link, association, content) ->
  new_id = new Date().getTime()
  regexp = new RegExp("new_" + association, "g")
  $(link).before content.replace(regexp, new_id)
