#= require ./index
App.Methods.Monsters = {}

App.Methods.Monsters.remove_fields = (link) ->
  $(link).parents('div.row.col-form-label').remove()
  false

App.Methods.Monsters.add_fields = (link, association, content) ->
  $('.monster_rows:last-child').append(content)
