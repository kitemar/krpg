$.widget 'custom.catcomplete', $.ui.autocomplete,
  _create: ->
    @_super()
    @widget().menu 'option', 'items', '> :not(.ui-autocomplete-category)'
  _renderMenu: (ul, items) ->
    that = this
    currentCategory = ''
    $.each items, (index, item) ->
      li = undefined
      if item.type != currentCategory
        ul.append '<li class=\'ui-autocomplete-category\'>' + item.type + '</li>'
        currentCategory = item.type
      li = that._renderItemData(ul, item)
      if item.type
        li.attr 'aria-label', item.type + ' : ' + item.name
      li.append '<hr/>'
  _renderItem: (ul, item) ->
    ul.addClass 'autocomplete-dropdown-menu'
    $('<li>').append($('<a class=\'suggestion\'>').text(item.name)).append('<span class=\'search-topic--' + item.status + '\'>').appendTo ul

$(document).on 'ready', ->
  anchor_id = window.location.hash
  $('#search').catcomplete
    source: '/autocomplete.json',
    minLength: 2,
    focus: (event, ui) ->
      @value = ui.item.name
      event.preventDefault()
    select: (e, ui) ->
      if ui.item.anchor_resourсe != 'characters'
        link = '/' + ui.item.anchor_resourсe + '#' + ui.item.anchor_link
      else
        link = '/' + 'monsters' + '/' + ui.item.anchor_resourсe_id
      if anchor_id != '' # FIXME
        window.location.href = link
        $('.card-block').removeClass('in')
        $('#' + ui.item.anchor_link).closest('.card-block').addClass('in')
      else
        window.location.href = link
        $('#' + ui.item.anchor_link).closest('.card-block').addClass('in')
  if anchor_id != ''
    $('.card-block').removeClass('in')
    $(anchor_id).closest('.card-block').addClass('in')
