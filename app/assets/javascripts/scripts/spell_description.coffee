$(document).on 'change', '#character_spell_spell_id', (e) ->
  $.ajax
    url: "/spells/#{e.target.value}"
    dataType: 'script'
    method: 'GET'
