$(document).on 'keydown', '#char_point_points', (e) ->
  $(e.currentTarget).parents('form').submit() if e.keyCode == 13

$(document).on 'blur', '#char_point_points', (e) ->
  $(e.currentTarget).parents('form').submit()
