$(document).on 'change', '#spell_group_group', (e) ->
  $.ajax
    url: "/spells?group=#{e.currentTarget.value}"
    dataType: 'script'
    method: 'GET'
