$(document).on 'change', '#character_archetype_archetype_id', (e) ->
  $.ajax
    url: "/archetypes/#{e.target.value}"
    dataType: 'script'
    method: 'GET'

$(document).on 'change', '#character_race_id', (e) ->
  $.ajax
    url: "/races/#{e.target.value}"
    dataType: 'script'
    method: 'GET'
