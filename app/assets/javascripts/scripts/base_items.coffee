$(document).on 'change', '#parent_item_kind', (e) ->
  $.ajax
    url: "/items?kind=#{e.currentTarget.value}"
    dataType: 'script'
    method: 'GET'

$(document).on 'change', '#parent_item_item', (e) ->
  $.ajax
    url: "/items/#{e.target.value}"
    dataType: 'script'
    method: 'GET'
