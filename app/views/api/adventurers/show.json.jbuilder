json.info do
  json.id           adventurer.id
  json.name         adventurer.name
  json.speed        adventurer.value_for('speed', :points)
  json.initiative   adventurer.initiative_bonus
end
