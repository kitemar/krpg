json.adventurers do
  json.array! adventurers do |adventurer|
    json.id         adventurer.id
    json.name       adventurer.name
  end
end
