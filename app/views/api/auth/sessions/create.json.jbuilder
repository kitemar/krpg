if resource.errors.empty?
  json.auth_token  resource.authentication_token
  json.user do
    json.extract! resource.user, :email, :name
  end
else
  json.error resource.errors.full_messages.to_sentence
end
