class CreateCharPoints < ActiveRecord::Migration[5.0]
  def change
    create_table :char_points do |t|
      t.integer :points, null: false, default: 0
      t.integer :experience, default: 0
      t.references :character
      t.references :mastery

      t.timestamps
    end
  end
end
