class AddPlayableToTrait < ActiveRecord::Migration[5.0]
  def change
    add_column :traits, :playable, :boolean, null: false, default: true
  end
end
