class CreateMasteries < ActiveRecord::Migration[5.0]
  def change
    create_table :masteries do |t|
      t.string  :code
      t.string  :type

      t.timestamps
    end
  end
end
