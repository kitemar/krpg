class ChangeReleaseNotesToNotes < ActiveRecord::Migration[5.0]
  def up
    rename_table :release_notes, :notes
    add_column :notes, :owner_id, :integer
    add_column :notes, :type, :string

    execute(
      "UPDATE notes SET owner_id = u.id FROM users u WHERE u.admin IS TRUE"
    )

    execute(
      "UPDATE notes SET type = 'ReleaseNote'"
    )
  end

  def down
    remove_column :notes, :owner_id
    remove_column :notes, :type
    rename_table :notes, :release_notes
  end
end
