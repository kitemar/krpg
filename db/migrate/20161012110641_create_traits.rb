class CreateTraits < ActiveRecord::Migration[5.0]
  def change
    create_table :traits do |t|
      t.string     :title
      t.text       :description
      t.string     :condition
      t.integer    :points, defaul: 0
      t.references :mastery

      t.timestamps
    end
  end
end
