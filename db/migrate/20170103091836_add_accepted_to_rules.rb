class AddAcceptedToRules < ActiveRecord::Migration[5.0]
  def change
    add_column :rules, :accepted, :boolean
  end
end
