class AddAuthenticationTokenToIdentities < ActiveRecord::Migration[5.0]
  def change
    add_column :identities, :authentication_token, :string, limit: 30
    add_index :identities, :authentication_token, unique: true
  end
end
