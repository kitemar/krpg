class CreateCharacterSpells < ActiveRecord::Migration[5.0]
  def change
    create_table :character_spells do |t|
      t.references :character
      t.references :spell
    end
  end
end
