class CreateCharacters < ActiveRecord::Migration[5.0]
  def change
    create_table :characters do |t|
      t.string     :name, null: false
      t.references :user
      t.references :race
      t.boolean    :playable,    null: false, default: false
      t.integer    :hit_points,  null: false, default: 0
      t.integer    :soul_points, null: false, default: 0
      t.string     :type

      t.timestamps
    end
  end
end
