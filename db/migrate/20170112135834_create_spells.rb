class CreateSpells < ActiveRecord::Migration[5.0]
  def change
    create_table :spells do |t|
      t.string      :title, null: false
      t.string      :group,  null: false
      t.text        :description
      t.boolean     :accepted, default: false

      t.timestamps
    end
  end
end
