class CreateEncounters < ActiveRecord::Migration[5.0]
  def change
    create_table :encounters do |t|
      t.string :title, null: false, default: ''
      t.text :monsters, default: '[]'
      t.timestamps
    end
  end
end
