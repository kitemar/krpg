class AddProposer < ActiveRecord::Migration[5.0]
  def change
    add_column :rules, :proposer_id, :integer
    add_column :traits, :proposer_id, :integer
    add_column :spells, :proposer_id, :integer
  end
end
