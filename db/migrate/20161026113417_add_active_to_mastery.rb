class AddActiveToMastery < ActiveRecord::Migration[5.0]
  def change
    add_column :masteries, :active, :boolean, null: false, default: true
  end
end
