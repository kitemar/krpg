class CreateCharacterTraits < ActiveRecord::Migration[5.0]
  def change
    create_table :character_traits do |t|
      t.references :character
      t.references :trait
    end
  end
end
