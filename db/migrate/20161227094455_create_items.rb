class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string      :title
      t.string      :effect
      t.string      :kind
      t.string      :properties
      t.decimal     :weight, precision: 7, scale: 2, null: false
      t.decimal     :price, precision: 10, scale: 2, null: false
      t.integer     :quantity, default: 0
      t.boolean     :active, default: true
      t.boolean     :default, default: false
      t.references  :character
      t.string      :type

      t.timestamps
    end
  end
end
