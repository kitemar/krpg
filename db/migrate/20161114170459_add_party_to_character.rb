class AddPartyToCharacter < ActiveRecord::Migration[5.0]
  def change
    add_reference :characters, :party
  end
end
