class AddChatIdToIdentity < ActiveRecord::Migration[5.0]
  def change
    add_column :identities, :chat_id, :string
    add_index :identities, :chat_id, unique: true
  end
end
