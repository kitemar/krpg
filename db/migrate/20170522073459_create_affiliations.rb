class CreateAffiliations < ActiveRecord::Migration[5.0]
  def up
    create_table :affiliations do |t|
      t.references :party,     null: false
      t.references :character, null: false
      t.boolean    :active,    null: false, default: false
    end

    execute(
      "INSERT INTO affiliations (character_id, party_id) SELECT id, party_id FROM characters WHERE (characters.party_id IS NOT NULL)"
    )

    remove_column :characters, :party_id
  end

  def down
    add_column :characters, :party_id, :integer

    execute(
      "UPDATE characters SET party_id = p.party_id FROM affiliations p WHERE p.character_id = characters.id"
    )

    drop_table :affiliations
  end
end
