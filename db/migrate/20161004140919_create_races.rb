class CreateRaces < ActiveRecord::Migration[5.0]
  def change
    create_table :races do |t|
      t.string :code, null: false
      t.boolean :playable, default: false

      t.timestamps
    end
  end
end
