class AddGroupToRules < ActiveRecord::Migration[5.0]
  def change
    add_column :rules, :group, :string
  end
end
