class AddDescriptionToCharacters < ActiveRecord::Migration[5.0]
  def up
    add_column :characters, :description, :text, default: ''
    execute(
      "UPDATE characters SET description = ''"
    )
  end

  def down
    remove_column :characters, :description
  end
end
