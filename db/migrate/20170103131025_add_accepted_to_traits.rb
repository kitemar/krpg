class AddAcceptedToTraits < ActiveRecord::Migration[5.0]
  def change
    add_column :traits, :accepted, :boolean
  end
end
