class CreateCharacterArchetypes < ActiveRecord::Migration[5.0]
  def change
    create_table :character_archetypes do |t|
      t.references :character
      t.references :archetype
    end
  end
end
