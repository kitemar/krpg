require 'yaml'

masteries_list = YAML::load_file('./db/seed/masteries_list.yml')
races_list = YAML::load_file('./db/seed/races_list.yml')
played_traits_list = YAML::load_file('./db/seed/traits/played_list.yml')
not_played_traits_list = YAML::load_file('./db/seed/traits/not_played_list.yml')
race_archetypes_list = YAML::load_file('./db/seed/traits/race_archetypes_list.yml')
rules_list = YAML::load_file('./db/seed/rules_list.yml')
default_item_list = YAML::load_file('./db/seed/items/default_item_list.yml')
weapon_list = YAML::load_file('./db/seed/items/weapon_list.yml')
armor_list = YAML::load_file('./db/seed/items/armor_list.yml')
equipment_list = YAML::load_file('./db/seed/items/equipment_list.yml')

p 'Creating masteries'
masteries_list.each do |type, group|
  group.each do |code|
    Mastery.find_or_create_by(code: code, type: type.capitalize)
  end
end

p 'Creating races'
races_list.each do |type, race|
  race.each do |code|
    Race.find_or_create_by(code: code, playable: true)
    Archetype.find_or_create_by(name: "#{code}_archetype", race_id: Race.find_by(code: code).id, playable: false)
  end
end

p 'Creating played traits'
played_traits_list.each do |mastery, traits|
  traits.each do |trait|
    Trait.create_with(description: trait['description'],
                      condition: trait['condition'],
                      mastery: Mastery.find_by(code: mastery),
                      points: trait['points'],
                      playable: trait['playable'],
                      accepted: true).find_or_create_by(title: trait['title'])
  end
end

p 'Creating not played traits'
not_played_traits_list.each do |mastery, traits|
  traits.each do |trait|
    Trait.create_with(description: trait['description'],
                      condition: trait['condition'],
                      mastery: Mastery.find_by(code: mastery),
                      points: trait['points'],
                      playable: false,
                      accepted: true).find_or_create_by(title: trait['title'])
  end
end

p 'Creating race traits'
race_archetypes_list.each do |key, value|
  value.each do |trait|
    CharacterTrait.find_or_create_by(character: Archetype.find_by(race: Race.find_by(code: key)), trait: Trait.find_by(title: trait))
  end
end

p 'Creating rules'
rules_list.each do |rule|
  Rule.create_with(group: rule['group'],
                   description: rule['description'],
                   accepted: true).find_or_create_by(title: rule['title'])
end

p 'Creating base items'
default_item_list.each do |item|
  BaseItem.create_with(kind: item['kind'],
                       weight: item['weight'],
                       price: item['price'],
                       properties: item['properties'],
                       effect: item['effect'],
                       default: true,
                       active: true).find_or_create_by(title: item['title'])
end

weapon_list.each do |item|
  BaseItem.create_with(kind: item['kind'],
                       weight: item['weight'],
                       price: item['price'],
                       properties: item['properties'],
                       effect: item['effect'],
                       default: false,
                       active: true).find_or_create_by(title: item['title'])
end

armor_list.each do |item|
  BaseItem.create_with(kind: item['kind'],
                       weight: item['weight'],
                       price: item['price'],
                       properties: item['properties'],
                       effect: item['effect'],
                       default: false,
                       active: true).find_or_create_by(title: item['title'])
end

equipment_list.each do |item|
  BaseItem.create_with(kind: item['kind'],
                       weight: item['weight'],
                       price: item['price'],
                       properties: item['properties'],
                       effect: item['effect'],
                       default: false,
                       active: true).find_or_create_by(title: item['title'])
end

p 'Seeds are finished'
