# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170623151031) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "affiliations", force: :cascade do |t|
    t.integer "party_id",                     null: false
    t.integer "character_id",                 null: false
    t.boolean "active",       default: false, null: false
    t.index ["character_id"], name: "index_affiliations_on_character_id", using: :btree
    t.index ["party_id"], name: "index_affiliations_on_party_id", using: :btree
  end

  create_table "char_points", force: :cascade do |t|
    t.integer  "points",       default: 0, null: false
    t.integer  "experience",   default: 0
    t.integer  "character_id"
    t.integer  "mastery_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["character_id"], name: "index_char_points_on_character_id", using: :btree
    t.index ["mastery_id"], name: "index_char_points_on_mastery_id", using: :btree
  end

  create_table "character_archetypes", force: :cascade do |t|
    t.integer "character_id"
    t.integer "archetype_id"
    t.index ["archetype_id"], name: "index_character_archetypes_on_archetype_id", using: :btree
    t.index ["character_id"], name: "index_character_archetypes_on_character_id", using: :btree
  end

  create_table "character_spells", force: :cascade do |t|
    t.integer "character_id"
    t.integer "spell_id"
    t.index ["character_id"], name: "index_character_spells_on_character_id", using: :btree
    t.index ["spell_id"], name: "index_character_spells_on_spell_id", using: :btree
  end

  create_table "character_traits", force: :cascade do |t|
    t.integer "character_id"
    t.integer "trait_id"
    t.index ["character_id"], name: "index_character_traits_on_character_id", using: :btree
    t.index ["trait_id"], name: "index_character_traits_on_trait_id", using: :btree
  end

  create_table "characters", force: :cascade do |t|
    t.string   "name",                        null: false
    t.integer  "user_id"
    t.integer  "race_id"
    t.boolean  "playable",    default: false, null: false
    t.integer  "hit_points",  default: 0,     null: false
    t.integer  "soul_points", default: 0,     null: false
    t.string   "type"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "accepted",    default: false
    t.text     "description", default: ""
    t.index ["race_id"], name: "index_characters_on_race_id", using: :btree
    t.index ["user_id"], name: "index_characters_on_user_id", using: :btree
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree
  end

  create_table "encounters", force: :cascade do |t|
    t.string   "title",      default: "",   null: false
    t.text     "monsters",   default: "[]"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "locked_at"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "authentication_token",   limit: 30
    t.string   "chat_id"
    t.index ["authentication_token"], name: "index_identities_on_authentication_token", unique: true, using: :btree
    t.index ["chat_id"], name: "index_identities_on_chat_id", unique: true, using: :btree
    t.index ["email"], name: "index_identities_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_identities_on_reset_password_token", unique: true, using: :btree
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "items", force: :cascade do |t|
    t.string   "title"
    t.string   "effect"
    t.string   "kind"
    t.string   "properties"
    t.decimal  "weight",       precision: 7,  scale: 2,                 null: false
    t.decimal  "price",        precision: 10, scale: 2,                 null: false
    t.integer  "quantity",                              default: 0
    t.boolean  "active",                                default: true
    t.boolean  "default",                               default: false
    t.integer  "character_id"
    t.string   "type"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.index ["character_id"], name: "index_items_on_character_id", using: :btree
  end

  create_table "masteries", force: :cascade do |t|
    t.string   "code"
    t.string   "type"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "active",     default: true, null: false
  end

  create_table "notes", force: :cascade do |t|
    t.string   "title",      null: false
    t.text     "content",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "owner_id"
    t.string   "type"
  end

  create_table "parties", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.string   "searchable_type"
    t.integer  "searchable_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree
  end

  create_table "races", force: :cascade do |t|
    t.string   "code",                       null: false
    t.boolean  "playable",   default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "rules", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "group"
    t.boolean  "accepted"
    t.integer  "proposer_id"
  end

  create_table "spells", force: :cascade do |t|
    t.string   "title",                       null: false
    t.string   "group",                       null: false
    t.text     "description"
    t.boolean  "accepted",    default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "proposer_id"
  end

  create_table "traits", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "condition"
    t.integer  "mastery_id"
    t.integer  "points"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "playable",    default: true, null: false
    t.boolean  "accepted"
    t.integer  "proposer_id"
    t.index ["mastery_id"], name: "index_traits_on_mastery_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                       null: false
    t.string   "email",                      null: false
    t.boolean  "admin",      default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

end
